# Lite api Echonest

## Start

```javascript
var Echonest = require('echonest');
Echonest = new Echonest('API_KEY');
```

## EchoJS Compatibility

### GET Request
```javascript
Echonest.echo('playlist/dynamic/create').get({
  genre: '',
  type: 'genre-radio',
  adventurousness : 0
}, (err, json) => {
  if (err) {
    return;
  }
  console.log(json);
})
```

### POST Request
```javascript
Echonest.echo('playlist/dynamic/create').post({
  genre: '',
  type: 'genre-radio',
  adventurousness : 0
}, (err, json) => {
  if (err) {
    return;
  }
  console.log(json);
})
```

## Simplicity

```javascript
Echonest.next({
  results: 1,
  lookahead: 0,
  session_id : ''
}).then((json) => {
  console.log(json)
}))
```

```javascript
Echonest.create({
  genre: '',
  type: 'genre-radio',
  adventurousness : 0
}).then((json) => {
  console.log(json)
}))
```

```
...
```

## Get current echonest rates
```javascript
Echonest.getLimites()
```


## Tests

For the tests you need `jasmine` `npm install -g jasmine` and execute `jasmine` or `npm run test`.

## Other

### Ajax request

```javascript
this.ajax('www.exemple.com', '/states', { hello: 'world'}, "GET").then((json) => {
  console.log(json);
}.bind(this), function (fail) {
  console.log(fail);
});
```
