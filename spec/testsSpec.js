/**
 * Tests du module.
 */
var Echonest = require('../');


describe("Echonest", function() {
  it("Init", function () {
    Echonest = new Echonest("KEY_TESTS");
    expect(Echonest.ajax).toBeDefined();
    expect(Echonest.next).toBeDefined();
    expect(Echonest.create).toBeDefined();
    expect(Echonest.echo).toBeDefined();
    expect(Echonest.restart).toBeDefined();
  });

  it("Ajax", function(done) {
    Echonest.ajax('developer.echonest.com', '/api/v4/playlist/dynamic/next', {}, "GET")
      .then(function (result) {
        expect(result.reponse).not.toBeNull();
        done();
      }, function (fail) {
        done.fail('Fail on load ajax');
      });
    expect(true).toBe(true);
  });

  it("Next", function (done) {
    Echonest.next({}).then(function (result) {
      expect(result.reponse).not.toBeNull();
      done();
    }, function (fail) {
      done.fail('Fail on load NEXT');
    });
  });

  it("Create", function (done) {
    Echonest.create({}).then(function (result) {
      expect(result.reponse).not.toBeNull();
      done();
    }, function (fail) {
      done.fail('Fail on load Create');
    });
  });

  it("Echo", function (done) {
    Echonest.echo('playlist/dynamic/create').get({}, function (err, result) {
      if (err) {
        done.fail('Error on test ECHO:', err);
        return;
      }
      expect(result.reponse).not.toBeNull();
      done();
    })
  });


});
