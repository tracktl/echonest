"use strict"

// Api doc http://developer.echonest.com/docs/v4

var http        = require('http');
var querystring = require('querystring');

var config = {
  'base' : 'developer.echonest.com',
  'route' : '/api/v4/'
};

var Echonest = function (api_key) {
  this.api_key = api_key;
  return this;
};


Echonest.prototype.ajax = function (domaine, route, args, type) {
  return new Promise(function (resolve, fail) {

    var method  = type ? type : "GET"; // Par défault ont utilise GET.
    var query   = '';
    var headers = {};

    if (method === "POST") {
      args = Object.merge(args, { api_key : this.api_key });
      var postData = querystring.stringify(args);

      headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': postData.length
      };
    } else if (method === "GET") {
      query += '?api_key=' + this.api_key; // Adding api key in url
      for (var i=0; i<Object.keys(args).length; i++) {
        if (Object.keys(args)[i] == "genre") {

          var listgenresBrut = args[Object.keys(args)[i]];
          var listgenres = typeof listgenresBrut == "string" ?
            listgenresBrut.split(',') : listgenresBrut;

          for (var ig=0; ig<listgenres.length; ig++) {
            query += '&genre='+encodeURIComponent(listgenres[ig]);
          }
        } else {
          query += '&'+encodeURIComponent(Object.keys(args)[i])+'='+encodeURIComponent(args[Object.keys(args)[i]]);
        }
      }

      headers = {
        'Content-Type': 'application/json'
      };

      // GET console.log('URL: ', domaine+route+query)
    } else {
      console.log("Method not supported sry.");
      return;
    }

    var options = {
      hostname: domaine,
      path: route + query,
      method: method,
      headers: headers
    };

    var result = '';
    var req = http.request(options, function (res) {
      res.setEncoding('utf8');

      if(Object.keys(args).length && res.statusCode !== 200) {
        console.log('Warning request statusCode:', res.statusCode, 'Header:', res.headers);
        console.log('Url: ', options.hostname+options.path+ " ["+options.method+"]");
      }
      var resHeader = JSON.stringify(res.headers);
      this.setLimites(resHeader);

      res.on('data', function (data) {
        result += data;
      });

      res.on('end', function () {
        try {
          resolve(JSON.parse(result));
        } catch (e) {
          fail(e);
        }
      });

      res.on('error', function (e) {
        fail(e);
      });

    }.bind(this));

    req.on('error', function(e) {
      console.log('problem with request: ' + e.message);
    });


    if (method === "POST") {
      req.write(postData);
    }
    req.end();

  }.bind(this));

};


Echonest.prototype.next = function (args) {
  var route = config.route + 'playlist/dynamic/next';
  return this.ajax(config.base, route, args);
};

Echonest.prototype.create = function (args) {
  var route = config.route + 'playlist/dynamic/create';
  return this.ajax(config.base, route, args);
};

Echonest.prototype.restart = function (args) {
  var route = config.route + 'playlist/dynamic/restart';
  return this.ajax(config.base, route, args);
};

Echonest.prototype.getLimites = function () {
  return this.rate;
};

Echonest.prototype.setLimites = function (headerJSON) {
  this.rate = {
    limit : headerJSON['x-ratelimit-limit'],
    used : headerJSON['x-ratelimit-used']
  };
  return this;
};

/* Juste pour la compatibilité avec echojs */
Echonest.prototype.echo = function (addr) {
  return {
    get : function (args, callback) {
      var route = config.route + addr;
      this.ajax(config.base, route, args, "GET").then(function (json) {
        try {
          callback(false, json);
        } catch (e) {
          console.log('e', e);
        }
      }.bind(this), function (fail) {
        callback(fail);
      });
    }.bind(this),

    post : function (args, callback) {
      var route = config.route + addr;
      this.ajax(config.base, route, args, "POST").then(function (json) {
        try {
          callback(false, json);
        } catch (e) {
          console.log('e', e);
        }
      }.bind(this), function (fail) {
        callback(fail);
      });
    }.bind(this)
  };
};

module.exports = function (api_key) {
  return new Echonest(api_key);
}
